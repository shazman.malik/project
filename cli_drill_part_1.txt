1.
>cd pwd
>cd ~
>mkdir -p hello
>cd hello
>mkdir five one
>cd five
>mkdir six
>cd six
>touch c.txt
>mkdir seven
>touch error.log
>cd ../../one
>touch a.txt b.txt
>mkdir two
>cd two
>touch d.txt
>mkdir three
>cd three
>touch e.txt
>mkdir four
>cd four
>touch access.log

2.

>cd ~
>cd hello
>find . -name "*.log" -delete

3.

>cd ~
>cd hello/one
>echo "Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others." >>a.txt

4.

>cd ~
>cd hello
>rm -rf hello
